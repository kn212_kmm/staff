import React from 'react';
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {useForm} from "react-hook-form";
import {confirmOrder, fetch_cart_wares} from "../../redux/action_creators/ware_action_creator";
import {useNavigate} from "react-router-dom";

const ConfirmOrder = () => {
    const dispatch = useAppDispatch()
    const user = useAppSelector(state => state.authorizationReducer.user)
    const navigate = useNavigate()
    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
    } = useForm()
    const submitHandler = (data: any) => {
        dispatch(confirmOrder(data.Adress, () => {
            navigate("/")
        }))
        dispatch(fetch_cart_wares())
    }
    return (
        <div className={'container-fluid'}>
            <div className={'col-md-5 col-lg-4 m-auto'}>
                <div className={'my-5 border border-1 py-3'}>
                    <div className={'w-100 border-bottom border-1 py-3'}>
                        <h5 className="text-center">Wear | Order confirming</h5>
                        {/*<p className={"text-danger text-center"}>{server_errors ? server_errors : ""}</p>*/}
                    </div>
                    <form onSubmit={handleSubmit(submitHandler)}>
                        <div className={'mb-3 px-5'}>
                            <label htmlFor="adress">Adress</label>
                            <input className={'form-control '} {...register("Adress", {
                                required: true,
                                value: user?.adress
                            })} type="text" id={'adress'}/>
                        </div>
                        <div className={'mb-3 px-5'}>
                            <button type={'submit'} className={'btn btn-success'}>Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ConfirmOrder;