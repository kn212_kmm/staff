import React from 'react';
import { useAppDispatch } from '../../hooks/redux';
import { makeBackup } from '../../redux/action_creators/backup_action_creator';

const CabinetBackup = () => {
    const dispatch = useAppDispatch()

    const makeBackupHandler = () => {
        dispatch(makeBackup())
    }
    return (
        <div className={"mt-4"}>
            <button onClick={makeBackupHandler} className={"btn btn-danger mx-auto d-block"}>Make backup</button>
        </div>
    );
};

export default CabinetBackup;