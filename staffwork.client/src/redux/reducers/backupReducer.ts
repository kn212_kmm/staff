import { createSlice, PayloadAction } from "@reduxjs/toolkit"

interface IBackupsReducerState {
    isLoading: boolean,
    errors: any,
}

const initialState: IBackupsReducerState = {
    isLoading: false,
    errors: null
}

export const backupReducer = createSlice({
    name: "backupReducer",
    initialState: initialState,
    reducers: {
        BACKUP(state){
            state.isLoading = true
        },
        BACKUP_SUCCESS(state){
            state.isLoading = false
        },
        BACKUP_ERROR(state, action: PayloadAction<any>){
            state.isLoading = false
            state.errors = action.payload
        },
    }
})

export default backupReducer.reducer