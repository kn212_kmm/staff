import axios from "axios";
import {backupReducer} from "../reducers/backupReducer";
import { AppDispatch } from "../store";
import { GraphQlEndpoint } from "../../global_variables";
import {NotificationReducer} from "../reducers/NotificationReducer";
const {BACKUP, BACKUP_ERROR, BACKUP_SUCCESS} = backupReducer.actions
const {SHOW_ERROR_MESSAGE, SHOW_SUCCESS_MESSAGE, SHOW_WARNING_MESSAGE} = NotificationReducer.actions
export const makeBackup = () => async (dispatch: AppDispatch) => {
    const query = {
        'query': `
            mutation{
                backup{
                    backup{
                        errors
                        statusCode
                    }
                }
            }
        `
    }
    dispatch(BACKUP())
    try{
        const response = await axios({
            url: GraphQlEndpoint,
            method: 'post',
            withCredentials: true,
            headers: {
                "content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('accessToken')}`,
            },
            data: query
        })
        let r = response.data.data.backup.backup
        if (r.statusCode === 201){
            dispatch(SHOW_SUCCESS_MESSAGE("Successfully backuped!"))
        }else{
            dispatch(SHOW_ERROR_MESSAGE(r.errors))
        }
    }catch(e: any){
        dispatch(BACKUP_ERROR(e))
    }
}