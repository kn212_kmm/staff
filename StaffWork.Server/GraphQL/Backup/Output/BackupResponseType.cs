﻿using GraphQL.Types;

namespace StaffWork.Server.GraphQL.Backup.Output
{
    public class BackupResponseType : ObjectGraphType<BackupResponse>
    {
        public BackupResponseType()
        {
            Field<NonNullGraphType<IntGraphType>, int>()
             .Name("StatusCode")
             .Resolve(ctx => ctx.Source.StatusCode);

            Field<ListGraphType<StringGraphType>, List<string>?>()
              .Name("Errors")
              .Resolve(ctx => ctx.Source.Errors);
        }
    }
}
