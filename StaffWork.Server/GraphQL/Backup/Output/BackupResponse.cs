﻿namespace StaffWork.Server.GraphQL.Backup.Output
{
    public class BackupResponse
    {
        public int StatusCode { get; set; }
        public List<string> Errors { get; set; }

        public BackupResponse()
        {
            Errors = new List<string>();
        }
    }
}
