﻿using GraphQL.Types;
using StaffWork.Server.GraphQL.Backup.Output;
using StaffWork.Server.Providers.Interfaces;

namespace StaffWork.Server.GraphQL.Backup
{
    public class BackupMutation : ObjectGraphType
    {
        public BackupMutation(IBackupProvider provider)
        {
            Field<NonNullGraphType<BackupResponseType>, BackupResponse>()
                .Name("Backup")
                .Resolve(context =>
                {
                    return provider.Backup();
                }   
                );
        }
    }
}
