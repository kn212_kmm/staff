﻿using StaffWork.Server.GraphQL.Backup.Output;

namespace StaffWork.Server.Providers.Interfaces
{
    public interface IBackupProvider
    {
        BackupResponse Backup();
    }
}
