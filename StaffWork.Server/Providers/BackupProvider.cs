﻿using BusinessLogic;
using StaffWork.Server.GraphQL.Backup.Output;
using StaffWork.Server.Providers.Interfaces;

namespace StaffWork.Server.Providers
{
    public class BackupProvider : IBackupProvider
    {
        private readonly IBackupDataProvider _backupDataProvider;
        public BackupProvider(IBackupDataProvider backupDataProvider)
        {
            _backupDataProvider = backupDataProvider;
        }

        public BackupResponse Backup()
        {
            var response = new BackupResponse();
            try
            {
                var result = _backupDataProvider.MakeBackup($"C:\\Users\\dro1l\\source\\repos\\StaffWork\\MSSQLProvider\\backups\\{DateTime.Now.ToString("dd-M-yyyy_H-m-ss")}.bak");
                response.StatusCode = 201;
            }
            catch (Exception)
            {
                response.Errors.Add("Database error!");
                response.StatusCode = 500;
                throw;
            }
            return response;
        }
    }
}
