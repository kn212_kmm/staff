﻿using BusinessLogic;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSSQLProvider
{
    public class BackupDataProvider : IBackupDataProvider
    {
        private readonly string connectionString;
        public BackupDataProvider(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public bool MakeBackup(string backupPath)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                try
                {
                    var result = connection.Execute(@$"backup database [Staff] to disk = '{backupPath}' with format");
                    return true;
                }
                catch (Exception)
                {
                    return false;
                    throw;
                }

            }
        }
    }
}
